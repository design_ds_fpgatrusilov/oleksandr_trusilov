----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    13:01:00 11/19/2016 
-- Design Name: 
-- Module Name:    kyi_mux_32 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity kyi_mux_32 is
    Port ( data_1 : in  STD_LOGIC_VECTOR (31 downto 0);
           data_2 : in  STD_LOGIC_VECTOR (31 downto 0);
           data_0 : out  STD_LOGIC_VECTOR (31 downto 0);
           s0 : in  STD_LOGIC;
           CLK : in  STD_LOGIC);
end kyi_mux_32;

architecture Behavioral of kyi_mux_32 is
BEGIN
process(data_1,data_2,s0,CLK)
begin

 case s0 is

    when '0' => data_0 <= data_1;
    when '1' => data_0 <= data_2;
    when others => data_0 <= "ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ";
	 end case;

end process;



end Behavioral;

