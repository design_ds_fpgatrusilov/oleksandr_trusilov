----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    12:58:48 11/19/2016 
-- Design Name: 
-- Module Name:    bus_8_32 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity bus_8_32 is
    Port ( in_sig : in  STD_LOGIC_VECTOR (7 downto 0);
           out_sig : out  STD_LOGIC_VECTOR (31 downto 0));
end bus_8_32;

architecture Behavioral of bus_8_32 is

begin
	out_sig <= "000000000000000000000000" & in_sig;

end Behavioral;

