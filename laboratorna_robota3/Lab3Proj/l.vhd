----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    10:03:28 12/16/2017 
-- Design Name: 
-- Module Name:    l - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity l is
    Port ( l_a : in  STD_LOGIC_VECTOR (7 downto 0);
           l_out2 : out  STD_LOGIC_VECTOR (39 downto 0));
end l;

architecture Behavioral of l is
signal l_prod,l_p31,l_p22,
l_p20,l_p17,l_p14,l_p9,l_p3,l_p0:unsigned (39 downto 0);
begin
l_p0  <="00000000000000000000000000000000" & unsigned (l_A) & "";
l_p3  <="00000000000000000000000000000" & unsigned (l_A) & "000";
l_p9  <="00000000000000000000000" & unsigned (l_A) & "000000000";
l_p14 <="000000000000000000" & unsigned (l_A) & "00000000000000";
l_p17 <="000000000000000" & unsigned (l_A) & "00000000000000000";
l_p20 <="000000000000" & unsigned (l_A) & "00000000000000000000";
l_p22 <="0000000000" & unsigned (l_A) & "0000000000000000000000";
l_p31 <="0" & unsigned (l_A) & "0000000000000000000000000000000";
l_prod <= (l_p0 +l_p3 + l_p14 + l_p20 + l_p31 ) - l_p9 - l_p17 - l_p22;
l_out2 <= std_logic_vector (l_prod);
end Behavioral;

