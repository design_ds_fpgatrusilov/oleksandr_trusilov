----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    18:33:53 11/19/2016 
-- Design Name: 
-- Module Name:    kyi_m1 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity kyi_m1 is
    Port ( a : in  STD_LOGIC_VECTOR (7 downto 0);
           out1 : out  STD_LOGIC_VECTOR (15 downto 0));
end kyi_m1;

architecture Behavioral of kyi_m1 is

signal kyi_prod, kyi_p7, kyi_p4, kyi_p0:unsigned (15 downto 0);
begin
	kyi_p0  <="00000000" & unsigned (a);
	kyi_p4  <="0000" & unsigned (a) & "0000";
	kyi_p7  <="0" & unsigned (a) & "0000000";
	kyi_prod <= (kyi_p0 + kyi_p7) - kyi_p4;
	out1 <= std_logic_vector(kyi_prod);


end Behavioral;

