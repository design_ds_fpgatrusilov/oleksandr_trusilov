<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="virtex4" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="op1(16:0)" />
        <signal name="op2" />
        <signal name="op3" />
        <signal name="kyi_in(7:0)" />
        <signal name="C" />
        <signal name="CE" />
        <signal name="XLXN_8">
        </signal>
        <signal name="CLR" />
        <signal name="XLXN_10(7:0)" />
        <signal name="XLXN_12(7:0)" />
        <signal name="XLXN_13" />
        <signal name="XLXN_14" />
        <signal name="XLXN_15" />
        <signal name="op4(15:0)" />
        <port polarity="Output" name="op1(16:0)" />
        <port polarity="Output" name="op2" />
        <port polarity="Output" name="op3" />
        <port polarity="Input" name="kyi_in(7:0)" />
        <port polarity="Input" name="C" />
        <port polarity="Input" name="CE" />
        <port polarity="Input" name="CLR" />
        <port polarity="Output" name="op4(15:0)" />
        <blockdef name="kyi_f">
            <timestamp>2016-11-19T15:7:29</timestamp>
            <rect width="512" x="32" y="32" height="204" />
            <line x2="32" y1="80" y2="80" style="linewidth:W" x1="0" />
            <line x2="544" y1="80" y2="80" style="linewidth:W" x1="576" />
            <line x2="544" y1="140" y2="140" x1="576" />
            <line x2="544" y1="192" y2="192" x1="576" />
            <line x2="36" y1="144" y2="144" x1="4" />
        </blockdef>
        <blockdef name="kyi_sch2">
            <timestamp>2016-11-19T15:29:6</timestamp>
            <rect width="64" x="0" y="20" height="24" />
            <line x2="0" y1="32" y2="32" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-236" height="24" />
            <line x2="384" y1="-224" y2="-224" x1="320" />
            <rect width="256" x="64" y="-256" height="320" />
        </blockdef>
        <block symbolname="kyi_f" name="XLXI_2">
            <blockpin signalname="kyi_in(7:0)" name="din(7:0)" />
            <blockpin signalname="op1(16:0)" name="dout(16:0)" />
            <blockpin signalname="op2" name="rfd" />
            <blockpin signalname="op3" name="rdy" />
            <blockpin signalname="C" name="clk" />
        </block>
        <block symbolname="kyi_sch2" name="XLXI_3">
            <blockpin signalname="CE" name="CE" />
            <blockpin signalname="C" name="C" />
            <blockpin signalname="CLR" name="CLR" />
            <blockpin signalname="op4(15:0)" name="res(15:0)" />
            <blockpin signalname="kyi_in(7:0)" name="kui_1(7:0)" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="1104" y="400" name="XLXI_2" orien="R0">
        </instance>
        <branch name="op1(16:0)">
            <wire x2="1712" y1="480" y2="480" x1="1680" />
        </branch>
        <iomarker fontsize="28" x="1712" y="480" name="op1(16:0)" orien="R0" />
        <branch name="op2">
            <wire x2="1712" y1="544" y2="544" x1="1680" />
        </branch>
        <iomarker fontsize="28" x="1712" y="544" name="op2" orien="R0" />
        <branch name="op3">
            <wire x2="1712" y1="592" y2="592" x1="1680" />
        </branch>
        <iomarker fontsize="28" x="1712" y="592" name="op3" orien="R0" />
        <branch name="kyi_in(7:0)">
            <wire x2="1040" y1="480" y2="480" x1="992" />
            <wire x2="1040" y1="480" y2="896" x1="1040" />
            <wire x2="1120" y1="896" y2="896" x1="1040" />
            <wire x2="1120" y1="896" y2="1200" x1="1120" />
            <wire x2="1248" y1="1200" y2="1200" x1="1120" />
            <wire x2="1104" y1="480" y2="480" x1="1040" />
        </branch>
        <iomarker fontsize="28" x="992" y="480" name="kyi_in(7:0)" orien="R180" />
        <branch name="C">
            <wire x2="960" y1="544" y2="544" x1="832" />
            <wire x2="960" y1="544" y2="1024" x1="960" />
            <wire x2="1072" y1="1024" y2="1024" x1="960" />
            <wire x2="1072" y1="1024" y2="1072" x1="1072" />
            <wire x2="1248" y1="1072" y2="1072" x1="1072" />
            <wire x2="1104" y1="544" y2="544" x1="960" />
        </branch>
        <branch name="CE">
            <wire x2="1072" y1="960" y2="960" x1="880" />
            <wire x2="1072" y1="960" y2="1008" x1="1072" />
            <wire x2="1248" y1="1008" y2="1008" x1="1072" />
        </branch>
        <branch name="CLR">
            <wire x2="1072" y1="1088" y2="1088" x1="976" />
            <wire x2="1072" y1="1088" y2="1136" x1="1072" />
            <wire x2="1248" y1="1136" y2="1136" x1="1072" />
        </branch>
        <iomarker fontsize="28" x="976" y="1088" name="CLR" orien="R180" />
        <iomarker fontsize="28" x="880" y="960" name="CE" orien="R180" />
        <iomarker fontsize="28" x="832" y="544" name="C" orien="R180" />
        <instance x="1248" y="1168" name="XLXI_3" orien="R0">
        </instance>
        <iomarker fontsize="28" x="1808" y="928" name="op4(15:0)" orien="R0" />
        <branch name="op4(15:0)">
            <wire x2="1712" y1="944" y2="944" x1="1632" />
            <wire x2="1712" y1="928" y2="944" x1="1712" />
            <wire x2="1808" y1="928" y2="928" x1="1712" />
        </branch>
    </sheet>
</drawing>