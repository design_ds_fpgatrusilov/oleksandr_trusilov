The following files were generated for 'kyi_f' in directory
D:\fpga_labs\kyi_lab3\ipcore_dir\

Opens the IP Customization GUI:
   Allows the user to customize or recustomize the IP instance.

   * kyi_f.mif

XCO file generator:
   Generate an XCO file for compatibility with legacy flows.

   * kyi_f.xco

Creates an implementation netlist:
   Creates an implementation netlist for the IP.

   * kyi_f.ngc
   * kyi_f.vhd
   * kyi_f.vho
   * kyi_fCOEFF_auto0_0.mif
   * kyi_fCOEFF_auto0_1.mif
   * kyi_fCOEFF_auto0_2.mif
   * kyi_ffilt_decode_rom.mif

Creates an HDL instantiation template:
   Creates an HDL instantiation template for the IP.

   * kyi_f.vho

IP Symbol Generator:
   Generate an IP symbol based on the current project options'.

   * kyi_f.asy
   * kyi_f.mif

SYM file generator:
   Generate a SYM file for compatibility with legacy flows

   * kyi_f.sym

Generate ISE metadata:
   Create a metadata file for use when including this core in ISE designs

   * kyi_f_xmdf.tcl

Generate ISE subproject:
   Create an ISE subproject for use when including this core in ISE designs

   * kyi_f.gise
   * kyi_f.xise

Deliver Readme:
   Readme file for the IP.

   * kyi_f_readme.txt

Generate FLIST file:
   Text file listing all of the output files produced when a customized core was
   generated in the CORE Generator.

   * kyi_f_flist.txt

Please see the Xilinx CORE Generator online help for further details on
generated files and how to use them.

