--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   14:09:22 09/24/2016
-- Design Name:   
-- Module Name:   D:/fgpa_labs/KhYuI_lab1/KhYuI_m1_test.vhd
-- Project Name:  KhYuI_lab1
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: KhYuI_m1
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY KhYuI_m1_test IS
END KhYuI_m1_test;
 
ARCHITECTURE behavior OF KhYuI_m1_test IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT KhYuI_m1
    PORT(
         KhYuI_a : IN  std_logic;
         KhYuI_b : IN  std_logic;
         KhYuI_d : IN  std_logic;
         KhYuI_e : IN  std_logic;
         KhYuI_res : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal KhYuI_a : std_logic := '0';
   signal KhYuI_b : std_logic := '0';
   signal KhYuI_d : std_logic := '0';
   signal KhYuI_e : std_logic := '0';

 	--Outputs
   signal KhYuI_res : std_logic;
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
 
  
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: KhYuI_m1 PORT MAP (
          KhYuI_a => KhYuI_a,
          KhYuI_b => KhYuI_b,
          KhYuI_d => KhYuI_d,
          KhYuI_e => KhYuI_e,
          KhYuI_res => KhYuI_res
        );
	KhYuI_a<=not KhYuI_a after 10 ns;
	KhYuI_b<=not KhYuI_b after 20 ns;
	KhYuI_d<=not KhYuI_d after 40 ns;
	KhYuI_e<=not KhYuI_e after 80 ns;
      
		 tb : PROCESS
   BEGIN
      WAIT; -- will wait forever
   END PROCESS;
 

END;
