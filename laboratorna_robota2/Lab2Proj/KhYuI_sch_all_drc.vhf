--------------------------------------------------------------------------------
-- Copyright (c) 1995-2013 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____ 
--  /   /\/   / 
-- /___/  \  /    Vendor: Xilinx 
-- \   \   \/     Version : 14.7
--  \   \         Application : sch2hdl
--  /   /         Filename : KhYuI_sch_all_drc.vhf
-- /___/   /\     Timestamp : 10/15/2016 23:21:16
-- \   \  /  \ 
--  \___\/\___\ 
--
--Command: C:\Xilinx\14.7\ISE_DS\ISE\bin\nt64\unwrapped\sch2hdl.exe -sympath D:/fpga_labs/KhYuI-lab2/ipcore_dir -intstyle ise -family virtex4 -flat -suppress -vhdl KhYuI_sch_all_drc.vhf -w D:/fpga_labs/KhYuI-lab2/KhYuI_sch_all.sch
--Design Name: KhYuI_sch_all
--Device: virtex4
--Purpose:
--    This vhdl netlist is translated from an ECS schematic. It can be 
--    synthesized and simulated, but it should not be modified. 
--

library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity KhYuI_sch_all is
   port ( KhYuI_A  : in    std_logic_vector (7 downto 0); 
          KhYuI_B  : in    std_logic_vector (7 downto 0); 
          KhYuI_P1 : out   std_logic_vector (15 downto 0); 
          KhYuI_P2 : out   std_logic_vector (15 downto 0); 
          KhYuI_P3 : out   std_logic_vector (15 downto 0));
end KhYuI_sch_all;

architecture BEHAVIORAL of KhYuI_sch_all is
   component KhYuI_mult1
      port ( KhYuI_A : in    std_logic_vector (7 downto 0); 
             KhYuI_B : in    std_logic_vector (7 downto 0); 
             KhYuI_P : out   std_logic_vector (15 downto 0));
   end component;
   
   component KhYuI_mult2
      port ( KhYuI_A  : in    std_logic_vector (7 downto 0); 
             KhYuI_B  : in    std_logic_vector (7 downto 0); 
             KhYuI_mP : out   std_logic_vector (15 downto 0));
   end component;
   
   component KhYuI_mult3
      port ( a : in    std_logic_vector (7 downto 0); 
             b : in    std_logic_vector (7 downto 0); 
             p : out   std_logic_vector (15 downto 0));
   end component;
   
begin
   XLXI_4 : KhYuI_mult1
      port map (KhYuI_A(7 downto 0)=>KhYuI_A(7 downto 0),
                KhYuI_B(7 downto 0)=>KhYuI_B(7 downto 0),
                KhYuI_P(15 downto 0)=>KhYuI_P1(15 downto 0));
   
   XLXI_5 : KhYuI_mult2
      port map (KhYuI_A(7 downto 0)=>KhYuI_A(7 downto 0),
                KhYuI_B(7 downto 0)=>KhYuI_B(7 downto 0),
                KhYuI_mP(15 downto 0)=>KhYuI_P2(15 downto 0));
   
   XLXI_7 : KhYuI_mult3
      port map (a(7 downto 0)=>KhYuI_A(7 downto 0),
                b(7 downto 0)=>KhYuI_B(7 downto 0),
                p(15 downto 0)=>KhYuI_P3(15 downto 0));
   
end BEHAVIORAL;


